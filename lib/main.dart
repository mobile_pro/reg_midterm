import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';

void main() => runApp(
    DevicePreview(
      enabled: true,
      tools: [
        ...DevicePreview.defaultTools
      ],
      builder: (context) => MyApp()
    )
);


class MyApp extends StatelessWidget {
// This widget is the root of your application.<font></font>
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Nunito"),
      title: 'Reg_BUU',
      home: Scaffold(
        body: Container(
          color: Color(0xFF17203A),
          child: MyHomePage(),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<String> menu = ["เมนูหลัก","ผลการลงทะเบียน","ตารางเรียน","ตารางสอบ","ค่าใช้จ่าย","ประวัตินิสิต"];
  final List<String> menuIcon = ["home","results","calendar_stady","celendar_quiz","pay","history"];

  bool sidebarOpen = false;

  double y0ffset = 0;
  double x0ffset = 0;
  double pageScale = 1;

  int selectedMenu = 0;

  String pageTitle = "มหาวิทยาลัยบูรพา";

  void setSidebarState() {
    setState(() {
      x0ffset = sidebarOpen ? 265 : 0;
      y0ffset = sidebarOpen ? 80 : 0;
      pageScale = sidebarOpen ? 0.8 : 1;
    });
  }

  void setPageTitle() {
    switch(selectedMenu) {
      case 0:
        pageTitle = "เมนูหลัก";
        break;
      case 1:
        pageTitle = "ผลการลงทะเบียน";
        break;
      case 2:
        pageTitle = "ตารางเรียน";
        break;
      case 3:
        pageTitle = "ตารางสอบ";
        break;
      case 4:
        pageTitle = "ค่าใช้จ่าย";
        break;
      case 5:
        pageTitle = "ประวัตินิสิต";
        break;
    }
  }


  @override
  Widget build(BuildContext context){
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 0),
                  child: Container(
                    color: Colors.indigoAccent,
                    child: Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            sidebarOpen = true;
                            setSidebarState();
                            // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ));
                          },
                          child: Container(
                            padding: const EdgeInsets.all(10),
                            margin: const EdgeInsets.only(top: 9.5, bottom: 15),
                            child: Image.asset("assets/images/profile.png"),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(bottom: 10, left: 10),
                          child: Text(
                            "63160084 Apassara Wannawijit",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Expanded(
                    child: ListView.builder(
                        itemCount: menu.length,
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () {
                          sidebarOpen = false;
                          selectedMenu = index;
                          setPageTitle();
                          setSidebarState();
                          // setPageTitle();
                        },
                        child: Menubar(
                          menuicon: menuIcon[index],
                          menu: menu[index],
                          selected: selectedMenu,
                          position: index,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                child: Menubar(
                menuicon: "logout",
                menu: "ออกจากระบบ",
                selected: selectedMenu,
                position: menu.length + 1,
                ),
                ),
                ],
                ),
          ),
            AnimatedContainer(
              curve: Curves.easeInOut,
              duration: Duration(milliseconds: 200),
              transform: Matrix4.translationValues(x0ffset, y0ffset, 1.0)..scale(pageScale),
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: sidebarOpen ? BorderRadius.circular(20) : BorderRadius.circular(0),
              ),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(top: 40,left: 25),
                      height: 60,
                      child: Row(
                        children: <Widget>[
                          GestureDetector(
                          onTap: () {
                          sidebarOpen = !sidebarOpen;
                          setSidebarState();
                          },
                          child: Container(
                              margin: const EdgeInsets.only(bottom: 12,left: 15),
                              // padding: const EdgeInsets.all(10),
                              child: Icon(Icons.menu,size: 35,),
                          ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(bottom: 15,left: 75),
                            child: Text(
                              pageTitle,
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 100),
                      child: Row(
                        children: [
                          Image.asset("assets/images/commer.png"),
                          Text("ประชาสัมพันธ์",
                            style: TextStyle(
                                fontSize: 25,
                              color: Colors.red,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10,left: 18),
                      child: Row(
                        children: [
                          Text("กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียน",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10,left: 75),
                      child: Row(
                        children: [
                          Text("การชำระเงินค่าธรรมเนียมการศึกษา",
                            style: TextStyle(
                                fontSize: 18,
                           ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 110),
                      child: Row(
                        children: [
                          Text("ภาคปลาย ปีการศึกษา 2566",
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20,left: 40),
                      child: Row(
                        children: [
                          Icon(Icons.square_rounded),
                          Text("พิมพ์ใบแจ้งยอดชำระเงิน",
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20,left: 40, bottom: 10),
                      child: Row(
                        children: [
                          Icon(Icons.square_rounded),
                          Text("ภายในวันที่ 15 - 22 ธันวาคม 2566",
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                    Container(
                      margin: EdgeInsets.only(top: 20,left: 110),
                      child: Row(
                        children: [
                          Text("แบบประเมินความคิดเห็น",
                            style: TextStyle(
                              fontSize: 18,fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5,left: 75),
                      child: Row(
                        children: [
                          Text("การให้บริการของสำนักงานอธิการบดี",
                            style: TextStyle(
                              fontSize: 18,
                                fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20,left: 40),
                      child: Row(
                        children: [
                          Icon(Icons.circle_rounded),
                          Text("สำหรับนิสิต https://bit.ly/3cyvuuf",
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20,left: 40, bottom: 20),
                      child: Row(
                        children: [
                          Icon(Icons.circle_rounded),
                          Text("สำหรับผู้ปกครอง https://bit.ly/39uIUWa",
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                    Container(
                      margin: EdgeInsets.only(top: 20,left: 55.5),
                      child: Row(
                        children: [
                          Text("ระเบียบสำหรับแนบเบิกค่าเล่าเรียนบุตร",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20,left: 25),
                      child: Row(
                        children: [
                          Text("**สอบถามข้อมูลที่กองคลังฯ ชั้น 3 สำนักงานอธิการบดี",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.red,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20,left: 120),
                      child: Row(
                        children: [
                          Icon(Icons.phone_iphone_rounded),
                          Text("โทร 038-102157",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.red,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
            ),
        ],
      ),
    );
  }
}

class Menubar extends StatelessWidget {
  final String menu;
  final String menuicon;
  final int selected;
  final int position;
  Menubar({required this.menu,required this.menuicon, required this.selected, required this.position});


  @override
  Widget build(BuildContext context) {
    return Container(
      color: selected == position ? Color(0xFFBEBEBE) : Color(0xFF17203A),
      child: Row(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(10),
            // margin: const EdgeInsets.only(top: 15),
            child: Image.asset("assets/images/$menuicon.png"),
          ),
          Container(
            // padding: const EdgeInsets.all(15),
            margin: const EdgeInsets.only(top: 5,left: 5),
            child: Text(
              menu,
              style: TextStyle(
                color: Colors.white,
                fontSize: 18,
              ),
            ),
          ),
        ],
      ),
    );
  }
}




